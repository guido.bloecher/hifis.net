---
date: 2021-07-01
title: Tasks in July 2021
service: backbone
---

## AAI: Deprovisioning information from Helmholtz Identity Providers
Deprovisioning of users will be supported by the Helmholtz AAI. This includes a framework to query
deprovisioning information from IdPs to allow automatic deprovisioning in Helmholtz AAI. In case of
failure, user shall be contacted to confirm their account manually.


