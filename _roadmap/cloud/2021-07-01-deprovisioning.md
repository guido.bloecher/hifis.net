---
date: 2021-07-01
title: Tasks in Jul 2021
service: cloud
---

## Deprovisioning of users for at least one service and at least one using centre
The automatic deprovisioning of users is documented and technically implemented for at least one service and at least one service using centre.
