---
date: 2021-10-01
title: Tasks in Oct 2021
service: cloud
---

## First Services fully in operation modus
The integration of several services of the initial service portfolio is completed and they can be used according to their description and service level.  Therefore, these services immediately become fully integrated services when the legal framework is signed.
