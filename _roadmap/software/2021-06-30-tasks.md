---
date: 2021-06-30
title: Tasks in June 2021
service: software
---

## Publish training program for the second half of 2021 
The education and training work package publishes the offered training events for the second half of 2021 on <{% link events.md %}>.
