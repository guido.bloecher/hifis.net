---
title: "GitLab for Software Development in Teams"
layout: event
organizers:
  - schlauch
lecturers:
  - stoffers
type:   workshop
start:
    date:   "2020-12-15"
    time:   "09:00"
end:
    date:   "2020-12-17"
    time:   "13:00"
location:
    campus: "Online Event"
    room:   ""
fully_booked_out: true
registration_link: https://events.hifis.net/event/28/
registration_period:
    from:   "2020-10-05"
    to:     "2020-12-11"
excerpt: 
    "Using GitLab's collaboration and automation features effectively"
---

## Description

This workshop teaches you how to use
basic and advanced GitLab features to
collaborate with others on a software project.

You will learn how to 
* Effectively plan and track work using issues
* Review change suggestions with merge requests
* Create and document releases, and
* How to automate repetitive tasks with continuous integration pipelines.

We will complete a few simulated iterations
of the software development lifecycle in a demo project,
both alone and in a team.

### Goal

Help developers of medium and large software systems to use the
collaboration and automation features of GitLab in order to
improve sustainability in their projects.

### Requirements

Basic Git skills are needed.
A good and quick tutorial can be found in the 
[Software Carpentry's "Git Novice" episodes 1 to 9](https://swcarpentry.github.io/git-novice/).
Participants need to bring their own laptop
with a modern web browser and Git set up on the command line.
