---
title: Make Your Script Ready for Publication
layout: event
organizers:
  - belayneh
  - dolling
  - "Johannes Petereit"
lecturers:
  - dolling
type:   Workshop
start:
    date:   "2020-12-08"
    time:   "09:00"
end:
    date:   "2020-12-09"
    time:   "18:00"
location:
    campus: "Online Event"
registration_link: https://events.hifis.net/event/34/
registration_period:
    from:   "2020-11-10"
    to:     "2020-11-30"
excerpt:
    "The workshop covers topics of best practices for software publication."
---

## Goal

This workshop aims to teach researchers step by step how existing source code can be ready for publication.


## Content

The workshop contains practical sessions on how to: 
* Organize source code and automate processes using GitLab
* Set up pipenv, linter
* Test source code
* Add documentation
* Add license
* Prepare citation


## Requirements

No prior knowledge or experience is required.
