---
title: "Helmholtz Hacky Hour #29"
layout: event
organizers:
  - dworatzyk
type:      hacky-hour
start:
  date:   "2021-06-02"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "Online"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>What is it like to be an RSE?</strong>"
---
## What is it like to be an RSE?
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

The development and use of software has become fundamental to a growing number of research fields. 
Often the lack of expertise, time, support or professional recognition makes the development and maintenance of high-quality software quite difficult.
Research software engineers (RSEs) are trying to improve this situation by promoting best practices, raising awareness for the need of structural change, and offering practical support such as trainings and consulting.
In the next session, the HIFIS Software team will share their experience and invite you to discuss on what it is like to be an RSE.

We are looking forward to seeing you!
