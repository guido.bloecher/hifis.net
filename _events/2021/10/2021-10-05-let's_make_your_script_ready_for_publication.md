---
title: "Let's Make Your Script Ready for Publication"
layout: event
organizers:
  - schlauch
type: workshop
start:
  date:   "2021-10-05"
  time:   "09:00"
end:
  date:   "2021-10-06"
  time:   "13:00"
location:
  campus: "Online"
excerpt:  "This workshop shows a lightweight approach towards publication of research code."
registration_period:
  from:   "2021-09-13"
  to:     "2021-09-26"
registration_link:  "https://events.hifis.net/event/151/"
fully_booked_out:  "False"
---
## Goal

We will provide you with actionable advice about how to prepare your research code before publishing it or submitting it alongside a research publication.

## Content

This workshop will cover the the following topics:
- Code repository structuring
- Minimum coding practices
- Documentation
- Open source licensing
- Minimum software release practices
- Software citation

You learn to apply the presented strategies using either the example code or your own.

Please see the [workshop curriculum](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials#curriculum) for further details.

## Requirements

- Basic Git skills are required. A good and quick tutorial can be found in the 
[Software Carpentry's Git Novice episodes 1 to 9](https://swcarpentry.github.io/git-novice/).
- Participants require a computer equipped with a modern Web browser and their specific environment (e.g., Git client, editor) for working on their scripts. We will provide more detailed setup information before the workshop.

We are looking forward to seeing you!

