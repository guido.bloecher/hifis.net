---
title: Programming using Python
layout: event
organizers:
  - belayneh
lecturers: 
  - belayneh
  - dolling
  - "Fuchs, Hannes"
  - "Lüdtke, Stefan"
  - "Meeßen, Christian"
  - "Brinkmann, Nils"
type:   workshop
start:
    date:   "2021-04-27"
    time:   "09:00"
end:
    date:   "2021-04-28"
    time:   "14:00"
location:   "Online"
excerpt:
    "This basic Software Carpentry workshop aims to teach Python for scientists and PhD students."
registration_period:
  from:   "2021-04-12"
  to:     "2021-04-19"
registration_link:  "https://events.hifis.net/event/97/"
redirect_from:
  - events/2021/03/16/2021-04-27and28-python
  - events/2021/03/17/2021-04-27and28-python
---

## Goal

Introduce scientists and PhD students to Python programming.

## Content

A Software Carpentry workshop is conceptualized as a two-day event that covers
the basics of Python programming using multiple excersis:

* Python fundamentals
* Data visualization
* Introduction to loops and lists
* Working with multiple input files
* Conditionals and functions


## Requirements

Neither prior knowledge nor experience in Python is needed to attend this workshop.
A headset (or at least headphones) is required.

More details can be found on the [workshop page](https://swc-bb.git-pages.gfz-potsdam.de/swc-pages/2021-04-27-GFZ-virtual/)
Registration will open 2-3 weeks before the event. We are looking forward to seeing you!
