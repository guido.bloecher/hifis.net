---
title: Community Use Cases
title_image: mountains-nature-arrow-guide-66100.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
excerpt:
  "Community Use Cases of HIFIS Services successfully applied to Researcher's daily work."
---

<article class="salert alert-success">
This is a preliminary list of Community Use Cases of HIFIS Services successfully applied to Researcher's daily work. More will be added soon. Stay tuned!
</article>


<div class="flex-cards">
{%- assign posts = site.categories['Use Case'] | where: "lang", "en" -%}
{% for post in posts -%}
{% include cards/usecase_card_image.html post=post excerpt=true%}
{% endfor -%}
</div>

<h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
<p>
Feel free to <a href="{% link contact.md %}">contact us</a>.
</p>
