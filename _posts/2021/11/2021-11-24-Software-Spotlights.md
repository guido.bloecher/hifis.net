---
title: "Software Spotlights in Helmholtz"
title_image: adi-goldstein-mDinBvq1Sfg-unsplash.jpg
date: 2021-11-24
authors:
  - konrad
  - jandt
layout: blogpost
categories:
  - news
excerpt: >
  Helmholtz Software Spotlights

---

# Helmholtz Software Spotlights

The
[HIFIS Software Cluster]({% link services/index.md %})
and the
[Helmholtz Research Software Forum](https://os.helmholtz.de/en/open-science-in-the-helmholtz-association/open-research-software/)
foster Research Software Development throughout Helmholtz.

The aim is to increase quality, visibility and sustainability of Research Software Engineering in Helmholtz.

We queried all Helmholtz Centres and Research Communities to present their **most relevant software projects** that represent **top success stories** of Reseach Software Engineering in Helmholtz.
To this day, we received approx. **90 proposals** from all research fields.

[Write us](mailto:support@hifis.net) if you are interested on details for these.

#### Presentation on HIFIS Software Community Pages

In a continuous series, we are going to present an increasing number of these software projects on our
[**HIFIS Software Community**]({% link spotlights.md %}) pages.
The first set of project presentations has just gone online.
Have a look and feel free to give us feedback.

<i class="fas fa-bell"></i> **Stay tuned!** <i class="fas fa-bell"></i>

#### Comments and Suggestions

If you have suggestions or questions, please do not hesitate to contact us.

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>


