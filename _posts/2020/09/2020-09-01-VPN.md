---
title: "Start of Helmholtz VPN"
title_image: omar-flores-MOO6k3RaiwE-unsplash.jpg
date: 2020-09-01
authors:
  - jandt
layout: blogpost
categories:
  - news
excerpt_separator: <!--more-->
---

Coordinated by DESY, HIFIS fostered a service agreement with the German National Research and Education Network (DFN) 
on operating a a Helmholtz-wide Virtual Private Network (Helmholtz Backbone). 
The VPN shall cover the majority of Helmholtz centres.
<!--more--> 
Currently 14 centres have agreed on the contract.
Besides DESY, these are: AWI, GEOMAR, KIT, GSI, UFZ, HZB, GFZ, HZI, DKFZ, HZDR, hereon (formerly HZG), MDC, and 
HMGU (Deutsches Forschungszentrum für Gesundheit und Umwelt, Neuherberg). 

FZJ is fostering a separate agreement, in coordination with the other centres.

By September, the first pilot VPN installations are being set-up between DESY, DKFZ, KIT, and HZDR.

By January 2021, it is expected to have the first production Virtual Private Network (Helmholtz Backbone) 
connections running (see [HIFIS Roadmap]({% link roadmap/index.md %})).

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
